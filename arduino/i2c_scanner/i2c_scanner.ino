
#include "Wire.h"

#define SDAPIN (SDA_OLED)
#define SCLPIN (SCL_OLED)

void setup()
{
  Serial.begin(115200);
  pinMode(RST_OLED, OUTPUT);
  digitalWrite(RST_OLED, HIGH);
}

void loop()
{
  printf("I2C scanning with SDA=%d, CLK=%d\n", SDAPIN, SCLPIN);
  Wire.begin(SDAPIN, SCLPIN);
  int address;
  int foundCount = 0;
  for (address = 1; address < 127; address++) {
    Wire.beginTransmission(address);
    uint8_t error = Wire.endTransmission();
    if (error == 0) {
      foundCount++;
      printf("Found device at 0x%.2x\n", address);
    }
  }
  printf("Found %d I2C devices by scanning.\n", foundCount);
  delay(2000);
}
