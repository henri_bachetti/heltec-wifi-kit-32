
#include <WiFi.h>
#include <WiFiClient.h>
#include <WebServer.h>
#include <ESPmDNS.h>

#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

// put your ssid and password here
const char* ssid = "..........";
const char* password = "..........";

WebServer server(80);

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels
#define SCREEN_ADDRESS 0x3C
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire1, RST_OLED);

const int led = LED_BUILTIN;

void handleRoot() {
  int ledState = digitalRead(led);
  String state = ledState == LOW ? "OFF" : "ON";
  String message = "<style>html { font-family: Helvetica; display: inline-block; margin: 0px auto; text-align: center;}";
  message += ".button { background-color: #4CAF50; border: none; color: white; padding: 16px 40px;";
  message += "text-decoration: none; font-size: 30px; margin: 2px; cursor: pointer;}";
  message += ".button2 {background-color: #555555;}</style></head>";
  message += "<body><h1>HELTEC WIFI KIT 32 Web Server</h1>";
  message += "<p>LED 1 - State " + state + "</p>";
  if (ledState == false) {
    message += "<p><a href=\"/on\"><button class=\"button\">ON</button></a></p>";
  } else {
    message += "<p><a href=\"/off\"><button class=\"button button2\">OFF</button></a></p>";
  }

  message += "</body></html>";
  server.send(200, "text/html", message);
}

void handleNotFound() {
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i = 0; i < server.args(); i++) {
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
}

void WiFiStationConnected(WiFiEvent_t event, WiFiEventInfo_t info) {
  Serial.println("Connected to AP successfully!");
}

void WiFiGotIP(WiFiEvent_t event, WiFiEventInfo_t info) {
  digitalWrite(led, LOW);
  Serial.printf("WiFi connected to %s\n", ssid);
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  display.clearDisplay();
  display.setCursor(0, 0);
  display.print(F("Connected to "));
  display.setCursor(0, 10);
  display.print(ssid);
  display.setCursor(0, 20);
  display.print("IP: ");
  display.print(WiFi.localIP());
  display.display();
  Serial.println("");
}

void WiFiStationDisconnected(WiFiEvent_t event, WiFiEventInfo_t info) {
  digitalWrite(led, HIGH);
  Serial.println("Disconnected from WiFi access point");
  Serial.print("WiFi lost connection. Reason: ");
  Serial.println(info.disconnected.reason);
  Serial.printf("Trying to reconnect to %s\n", ssid);
  display.clearDisplay();
  display.setTextColor(SSD1306_WHITE); // Draw white text
  display.setCursor(0, 0);
  display.println(F("Reconnecting to "));
  display.setCursor(0, 10);
  display.println(ssid);
  display.display();
  WiFi.reconnect();
}

void setup(void)
{
  pinMode(led, OUTPUT);
  digitalWrite(led, HIGH);
  Serial.begin(115200);
  Wire1.begin(SDA_OLED, SCL_OLED);
  if(!display.begin(SSD1306_SWITCHCAPVCC, SCREEN_ADDRESS)) {
    Serial.println(F("SSD1306 allocation failed"));
    for(;;);
  }  
  display.setTextSize(1);
  display.clearDisplay();
  display.setTextColor(SSD1306_WHITE); // Draw white text
  display.setCursor(0, 0);
  display.println(F("Connecting to "));
  display.setCursor(0, 10);
  display.println(ssid);
  display.display();
  
  WiFi.onEvent(WiFiStationConnected, SYSTEM_EVENT_STA_CONNECTED);
  WiFi.onEvent(WiFiGotIP, SYSTEM_EVENT_STA_GOT_IP);
  WiFi.onEvent(WiFiStationDisconnected, SYSTEM_EVENT_STA_DISCONNECTED);
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  WiFi.begin(ssid, password);
  Serial.println("");
  Serial.print("Connecting to ");
  Serial.println(ssid);

  server.on("/", handleRoot);

  server.on("/inline", []() {
    server.send(200, "text/plain", "This works as well");
  });

  server.on("/on", []() {
    digitalWrite(led, HIGH);
    handleRoot();
  });

  server.on("/off", []() {
    digitalWrite(led, LOW);
    handleRoot();
  });

  server.onNotFound(handleNotFound);

  server.begin();
  Serial.println("HTTP server started");
}

void loop(void)
{
  server.handleClient();
}
